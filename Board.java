public class Board {
	
	//create fields
	private Tile[][] grid;
	private final int SIZE =3;
	
	//create constructors which uses a nested loops to pupulate or initialize the values inside the 2d array to be blank
	public Board(){
		//or this SIZE = 3;
		this.grid = new Tile[this.SIZE][this.SIZE];
		for(int i=0;i<this.grid.length;i++){
			for(int j=0;j<this.grid[i].length;j++){
				this.grid[i][j] = Tile.BLANK;
			}
		}
	}
	
	//create a toString method that returns a string that uses the Tile class to replace the blank values into X or O
	public String toString(){
		String builder ="";
		for(int i=0;i<this.grid.length;i++){
			builder += i + " ";
			for(int j=0;j<this.grid[i].length;j++){
				builder += grid[i][j].getName() + " ";
			}
			builder += "\n";
		}
		return builder;
	}
	
	//create a placeToken method that takes 3 parameters and returns a boolean. This method checks if the row and column is betweem 0 and 2 and if the 2d array
	//at a given position is equal to blank, it returns true; otherwise, it returns false.
	public boolean placeToken(int row, int col, Tile playerToken){
		boolean checker = (row >= 0 && row < grid.length && col >= 0 && col < grid.length);
		if(checker && this.grid[row][col] == Tile.BLANK){
			this.grid[row][col] = playerToken;
			return true;
		}
		else {
			return false;
		}
	}
	
	//This method takes no parameters and returns a boolean. This method performs a nested loop which check if there is a 2d array that is still blank, if so, it returns
	//false representing that the board isn't full yet; otherwise, it returns true
	public boolean checkIfFull(){
		for(int i =0;i<this.grid.length;i++){
			for(int j =0;j<this.grid[i].length;j++){
				if(this.grid[i][j] == Tile.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	//This method takes 1 parameter representing the playerToken and returns a boolean. This method checks if the row or the horizontal position has 3 X or 3 O
	// which indicate if a player wins, returns true; otherwise, returns false representing that there is not 3 matching X in the horizontal position.
	private boolean checkIfWinningHorizontal(Tile playerToken){
		if(grid[0][0] == playerToken && grid[0][1] == playerToken && grid[0][grid.length-1] == playerToken){
			return true;
		}
		else if(grid[1][0] == playerToken && grid[1][1] == playerToken && grid[1][grid.length -1] == playerToken){
			return true;
		}
		else if(grid[grid.length -1][0] == playerToken && grid[grid.length -1][1] == playerToken && grid[grid.length -1][grid.length -1] == playerToken){
			return true;
		}
		else {
			return false;
		}
	}
	
	//This method takes 1 parameter representing the playerToken and returns a boolean. This method checks if the column or the vertical position has 3 X or 3 O
	// which indicate if a player wins, returns true; otherwise, returns false representing that there is not 3 matching X or O in the horizontal position.
	private boolean checkIfWinningVertical(Tile playerToken){
		if(this.grid[0][0] ==playerToken && this.grid[1][0] ==playerToken&& this.grid[this.grid.length -1][0] ==playerToken){
			return true;
		}
		else if(this.grid[0][1] ==playerToken&& this.grid[1][1] ==playerToken&& this.grid[this.grid.length -1][1]==playerToken){
			return true;
		}
		else if(this.grid[0][this.grid.length -1] ==playerToken && this.grid[1][this.grid.length -1]==playerToken && this.grid[this.grid.length-1][this.grid.length-1]==playerToken){
			return true;
		}
		else {
			return false;
		}
	}
	
	//This method takes 1 parameter representing the playerToken and returns a boolean. It checks if whether one of the methods: winningHorizontal, winningvertical or winning diagonal
	//is true. if one of them is true, it returns true representing that a player has won or has matches 3 X or O in horizontal, vertical or diagonal position; otherwise, returns false
	public boolean checkIfWinning(Tile playerToken){
		return checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken);
	}
	
	//This method takes 1 parameter representing the playerToken and returns a boolean. This method checks if the diagonal position has 3 X or 3 O
	// which indicate if a player wins, returns true; otherwise, returns false representing that there is not 3 matching X or O in the diagonal position.
	private boolean checkIfWinningDiagonal(Tile playerToken){
		if(this.grid[0][0] ==playerToken && this.grid[1][1] ==playerToken&& this.grid[this.grid.length -1][this.grid.length-1] ==playerToken){
			return true;
		}
		else if(this.grid[this.grid.length-1][0] ==playerToken&& this.grid[1][1] ==playerToken&& this.grid[0][this.grid.length-1]==playerToken){
			return true;
		}
		else {
			return false;
		}
	}
}