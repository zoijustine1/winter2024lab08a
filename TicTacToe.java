import java.util.Scanner;
public class TicTacToe{
	public static void main(String[] args){
		runGame();
	}
	
	//This method goes in a loop until one of the player matches 3 X or 0 in horizontal, vertical or diagonal position or when the board if full and no match. 
	//There are 2 players: player 1 has tile X, whereas player 2 has Tile O. The players takes turn every loop. If a player enters an invalid row or column, it will 
	// in another loop until that player enters a valid input.
	public static void runGame(){
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to the Tic Tac Toe Game!\nPlayer 1's token: X\nPlayer 2's token: O");
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Tile playerToken = Tile.X;
		while(!gameOver){
			System.out.println("  " + 0 + " "  + 1 + " " + 2);
			System.out.println(board);
			System.out.println("Player " + player + ": It's your turn");
			int row = reader.nextInt();
			int column = reader.nextInt();
			if(player == 1){
				playerToken = Tile.X;
			}
			else {
				playerToken = Tile.O;
			}
			boolean checker = board.placeToken(row, column, playerToken);
			while(!checker){
				System.out.println("Invalid input. Enter a new row and column");
				row = reader.nextInt();
				column = reader.nextInt();
				checker = board.placeToken(row, column, playerToken);
			}
			if(board.checkIfWinning(playerToken)){
				System.out.println("Player " + player + " is the winner");
				gameOver = true;
			}
			else if(board.checkIfFull()){
				System.out.println("It's a tie");
				gameOver = true;
			}
			else {
				player++;
				if(player > 2){
					player = 1;
				}
			}
				
		}
	}
}