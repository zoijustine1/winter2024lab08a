public enum Tile{
	//create the fieds and initialize their constructor.
	X("X"),
	O("O"),
	BLANK("_");
	private final String name;
	
	//create a constructor that takes a String name and initializes the name field to the parameter
	private Tile(String name){
		this.name = name;
	}
	
	//create a get method and returns a String.
	public String getName(){
		return this.name;
	}
}